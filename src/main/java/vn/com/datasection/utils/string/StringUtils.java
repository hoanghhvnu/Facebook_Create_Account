package vn.com.datasection.utils.string;

/**
 * Created by hoanghh on 2/6/18.
 */
public class StringUtils {
    public static String randomString(String allowCharacters, int length) {
        return randomString(allowCharacters.toCharArray(), length);
    }

    public static String randomString(char[] chars, int length) {
        StringBuilder builder = new StringBuilder();
        while (length-- != 0) {
            int character = (int)(Math.random()*chars.length);
            builder.append(chars[character]);
        }
        return builder.toString();
    }
}
