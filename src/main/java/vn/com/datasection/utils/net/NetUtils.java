package vn.com.datasection.utils.net;

import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.StringJoiner;

/**
 * Created by hoanghh on 2/6/18.
 */
public class NetUtils {
    public static void main(String[] args) {
        try {
            String s = simpleRequest(new URL("https://ipinfo.io/json"));
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String simpleRequest(URL url) throws IOException {
        HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        InputStream is = conn.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);

        String inputLine;

        StringJoiner sj = new StringJoiner("");
        while ((inputLine = br.readLine()) != null) {
//            System.out.println(inputLine);
            sj.add(inputLine);
        }

        br.close();

        return new JSONObject(sj.toString()).toString();
    }
}
