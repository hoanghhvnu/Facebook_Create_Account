package com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by hoanghh on 2/6/18.
 */
public class Chrome {
    private static Random random = new Random();
    private static Logger logger = LoggerFactory.getLogger(Chrome.class);


    public static void main(String[] args) throws InterruptedException, IOException {
        int i = random.nextInt(4);
        System.out.println(i);

        logger.info("START APP");
        String firstName = "Nguyen";
        String lastName = "Dung";
        String username = "nguyendung29232";
        String password = "sdahf98we09";
        LocalDate birthday = LocalDate.parse("1990-01-12");
        String gender = "male";

        create(firstName, lastName, username, password, birthday, gender);


        logger.info("FINISH APP");
    }

    public static void create(String firstName, String lastName, String username, String password, LocalDate birthday, String gender) throws InterruptedException {
        String chromeDriverPath = "/data/app/chrome-driver/v2.34/chromedriver";
        logger.info("Chrome driver path: " + chromeDriverPath);

        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        /**
         * Temp mail
         */
//        System.setProperty("webdriver.gecko.driver", "/data/app/geckodriver");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://temp-mail.org/");
        driver.findElement(By.id("click-to-change")).click();

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector(".content input#mail")).sendKeys(username);
        /**
         * Email domain
         */
        // TODO domain here
        driver.findElement(By.cssSelector("#domain option:nth-child(" + (1 + random.nextInt(4)) + ")")).click();
        driver.findElement(By.id("postbut")).click();

        WebElement els = driver.findElement(By.cssSelector(".yourmail input#mail"));
        String email = els.getAttribute("value");

        logger.info("New email: " + email);
        driver.findElement(By.id("click-to-refresh")).click();
        TimeUnit.SECONDS.sleep(10);


        /**
         * Facebook
         */
        String proxy = "195.154.162.201:6743";
        logger.info("Proxy: " + proxy);
//        String ipinfo = NetUtils.simpleRequest(new URL("https://ipinfo.io/json"));
//        logger.info("IP info: " + ipinfo);

        System.setProperty("webdriver.chrome.driver", chromeDriverPath);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--proxy-server=socks5://" + proxy);
        WebDriver proxyDriver = new ChromeDriver(options);

//        WebDriver driver = new ChromeDriver();
//        proxyDriver.get("https://ipinfo.io/json");
//        TimeUnit.SECONDS.sleep(10);

        proxyDriver.navigate().to("https://ipinfo.io/json");
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.navigate().to("https://www.facebook.com/");

        proxyDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        proxyDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        proxyDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

        // add field
        proxyDriver.findElement(By.cssSelector("input[name=firstname]")).sendKeys(firstName);
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.findElement(By.cssSelector("input[name=lastname]")).sendKeys(lastName);
        TimeUnit.SECONDS.sleep(random.nextInt(5));

        proxyDriver.findElement(By.cssSelector("input[name=reg_email__]")).sendKeys(email);
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.findElement(By.cssSelector("input[name=reg_email_confirmation__]")).sendKeys(email);
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.findElement(By.cssSelector("input[name=reg_passwd__]")).sendKeys(password);
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.findElement(By.cssSelector("select[name=birthday_day] option:nth-child(" + (birthday.getDayOfMonth()  + 1) +")")).click();
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.findElement(By.cssSelector("select[name=birthday_month] option:nth-child(" + (birthday.getMonthValue() + 1 ) + ")")).click();
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        // TODO edit here
        proxyDriver.findElement(By.cssSelector("select[name=birthday_year] option:nth-child("  + (2018 - birthday.getYear() + 2) + ")")).click();
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        int genderValue = gender.equals("male") ? 1 : 2;
        proxyDriver.findElement(By.cssSelector("span[data-name=gender_wrapper] span:nth-child(" + genderValue + " ) input")).click();
        TimeUnit.SECONDS.sleep(random.nextInt(5));
        proxyDriver.findElement(By.cssSelector("button[name=websubmit]")).click();
        TimeUnit.SECONDS.sleep(random.nextInt(5));


        String currentUrl = proxyDriver.getCurrentUrl();
        logger.info("Current url: " + currentUrl);

//        if (currentUrl.contains("checkpoint/block")) {
//            return;
//        }

        // get code
        String responeCode = driver.findElement(By.cssSelector(".content tbody tr td:nth-child(2) a")).getText();
        logger.info("Verify code: " + responeCode);
        //		System.out.println(responeCode);
        String checkCode = responeCode.substring(0, 5);
        logger.info("Verify code: " + responeCode);
//		System.out.println(checkCode);
//
        proxyDriver.findElement(By.id("code_in_cliff")).sendKeys(checkCode);
//		System.out.println("before click confirm");
        proxyDriver.findElement(By.cssSelector(".clearfix button[name=confirm]")).click();
        proxyDriver.findElement(By.cssSelector(".topborder a")).click();
//
        driver.findElement(By.cssSelector(".clearfix a")).click();
        driver.findElement(By.cssSelector("div[class*=Footer] a::nth-child(1)")).click();
        driver.findElement(By.cssSelector(".clearfix a")).click();
//		System.out.println("Done");


        TimeUnit.SECONDS.sleep(30);
        proxyDriver.quit();
    }
}
